# Traffic Property App Plugin

```php
// add traffic_property_app.php to the theme content blocks
// where "acf_content_block_name" is the name of the acf content block
<?php load_app_to_content_block('acf_content_block_name'); ?>
```