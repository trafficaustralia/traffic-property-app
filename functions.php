<?php

//
// Load custom app acf blocks
//
require_once 'acf-json/index.php';

function traffic_rewrite()
{
    global $TrafficPropertyApp;

    $routes = $TrafficPropertyApp->setting('routes');
    $house = $routes['houseandland'];
    $land = $routes['landforsale'];
    $masterplan = $land['masterplan'];

    if (!empty($house) && !empty($house->ID)) {
        add_rewrite_rule(
          '^' . $house->post_name . '/(.*)',
          'index.php?page_id=' . $house->ID,
          'top'
      );
    }

    if (!empty($masterplan) && !empty($masterplan->ID)) {
        add_rewrite_rule(
          '^' . $masterplan->post_name . '/(.*)',
          'index.php?page_id=' . $masterplan->ID,
          'top'
      );
    }

    if (!empty($land) && !empty($land->ID)) {
        add_rewrite_rule(
          '^' . $land->post_name . '/(.*)',
          'index.php?page_id=' . $land->ID,
          'top'
      );
    }

    // add_rewrite_rule(
    //     '^' . $town->post_name . '/(.*)',
    //     'index.php?page_id='.$town_page,
    //     'top'
    // );

    // add_rewrite_rule(
    //     '^' . $ap->post_name . '/(.*)',
    //     'index.php?page_id='.$ap_page,
    //     'top'
    // );

    // add_rewrite_rule(
    //     '^'.self::HAL_SLUG.'(.*)',
    //     'index.php?page_id='.self::HAL_PAGE_ID.'&insite=true&is_url=$matches[1]',
    //     'top'
    // );
    /*
            add_rewrite_rule(
                '^'.self::LOC_SLUG.'(.*)',
                'index.php?page_id='.self::LOC_PAGE_ID.'&insite=true&is_url=$matches[1]',
                'top'
            );
    */
}
add_action('init', 'traffic_rewrite');

function property_app_enable_customization_menu() {
  global $TrafficPropertyApp;
  $TrafficPropertyApp->set_option('show_user_customisation_menu', true);
}

function property_app_load_settings() {
  add_action('wp_footer', function () {
      global $TrafficPropertyApp;
      global $tpa_acf_settings;
      global $type;

      $config = $TrafficPropertyApp->config();

      // if it's default then only load the main settings
      // otherwise add the acf_settings to the main settings
      if ($type !== 'default') {
          $config->acf_settings = $tpa_acf_settings;
      }

      echo '<script> window.traffic = ' . json_encode($config, JSON_UNESCAPED_SLASHES) . '; </script>';
  });
}

function load_app_to_content_block($content_block_key)
{
    $fields  = get_fields();
    $content_blocks = $fields[$content_block_key];
    $index = array_search('traffic_property_app', array_column($content_blocks, 'acf_fc_layout'));
    $content = $content_blocks[$index];

    global $tpa_acf_settings;
    $tpa_acf_settings = $content['settings'];
    $tpa_acf_settings = ($tpa_acf_settings) ? $tpa_acf_settings[0] : [];

    $type = $tpa_acf_settings['type'];
    if (!isset($type) || $type === 'default') {
      echo '<div id="traffic-app"></div>';
    } else {
      echo '<div id="traffic-app" data-app-type="acf-block" data-app-content="' . $type . '"></div>';
    }

    property_app_load_settings();
}
