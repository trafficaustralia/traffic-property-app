<?php

//======================================================================
// TRAFFIC PROPERTY APP
//======================================================================

$version = '1.1.18';

class TrafficPropertyApp
{
    public function __construct()
    {
      add_action('admin_menu', array($this, 'admin_menu'));

      $this->settings = [];

      $this->user_customization_menu = false;
      // add_action('wp_footer', array($this, 'load_app'));
    }

    public function set_option($name, $value = null)
    {
        $this->settings[$name] = $value;
    }

    public function setting($name)
    {
        $id = 'traffic-property-app';
        return get_field($name, $id);
    }

    public function config()
    {
        global $version;

        $enquiry_form = $this->setting('enquiry_form');
        $routes = $this->setting('routes');
        $labels = $this->setting('labels');
        $promotions = $this->setting('promotions');
        $estates = $this->setting('estates');
        $neighbourhoods = $this->setting('neighbourhoods');
        $api = $this->setting('api');
        $react_property_app = $api['react_property_app'];
        $stylesheet = $api['stylesheet'];
        $data_filter = $api['data_filter'];
        $custom_app = $api['custom_app']['url'];
        $latest_features = $api['latest_features'] === true;

        //
        // Load app script
        //
        if(!$react_property_app) {
          $app_url = $latest_features ? 'https://staging.property-app.traffic.com.au' : 'https://property-app.traffic.com.au';
          // if ($latest_features) $api['url'] = $app_url . '/api/payload.json?' . $version;

          if ($api['custom_app']['show'] && !empty($custom_app)) $app_url = $custom_app;
          wp_enqueue_script('traffic_property_app_js', $app_url . '/js/main.js', array(), $version, false);
        }

        //
        // Create config object
        //
        $config = new stdClass();
				$config->api = new stdClass();
        $config->api->url = $api['url'];
        $config->api->url = $api['url'];
        $config->api->view_count = $api['view_count'];
        $config->other_options = $this->setting('other_options');
        $show_user_customisation_menu = $this->settings['show_user_customisation_menu'];
        if($show_user_customisation_menu){
          $config->other_options['show_user_customisation_menu'] = $show_user_customisation_menu;
        }

        if (!empty($data_filter['type']) && !empty($data_filter['value'])) {
            $config->api->{$data_filter['type']} = $data_filter['value'];
        }

        //
        // Load styling
        //
        if(!$react_property_app) {
          $app_stylesheet = empty($api['stylesheet']) ? $app_url . '/css/main.css' : $app_url . '/css/' . $stylesheet . '.css';
          wp_enqueue_style('traffic_property_app_css', $app_stylesheet, array(), $version, false);
        }

        $style = $this->setting('style');
        $icons = $style['icons'];
        // $config->style->icons = $icons;

        //
        // Load icons
        //
        if (!$react_property_app && !empty($stylesheet)) {
            $getIconUrl = function ($name) use ($app_url, $stylesheet) {
                return $app_url . "/icons/" . $name . "." . $stylesheet . ".svg";
            };

            if (!empty($icons)) {
              foreach ($icons as $key => $icon) {
                $name = $icon['name'];
                $config->style->icons->{$name} = $getIconUrl($name);
              }
            } else {
              //
              // TODO: This should be removed after all sites have been updated
              //
              $config->style->icons->houseandland = $getIconUrl('houseandland');
              $config->style->icons->apartment = $getIconUrl('apartment');
              $config->style->icons->land = $getIconUrl('land');
              $config->style->icons->townhome = $getIconUrl('townhome');
              $config->style->icons->search = $getIconUrl('search');
              $config->style->icons->email = $getIconUrl('email');
              $config->style->icons->whatsapp = $getIconUrl('whatsapp');
              $config->style->icons->facebook = $getIconUrl('facebook');
              $config->style->icons->arrow_orange = $getIconUrl('arrow_orange');
            }
        }

        //
        // Load routes
        //
        if (!empty($routes)) {
            $config->routes = new stdClass();

            foreach ($routes as $key => $route) {
                if (!empty($route->post_name)) {
                    $config->routes->{$key} = $route->post_name;
                } elseif (!empty($route)) {
                    foreach ($route as $key2 => $route2) {
                        if (!empty($route2->post_name)) {
                            if (empty($config->routes->{$key})) {
                                $config->routes->{$key} = new stdClass();
                            }
                            $config->routes->{$key}->{$key2} = $route2->post_name;
                        }
                    }
                }
            }
        }

        //
        // Load estates color and label
        //
        if (!empty($estates)) {
            foreach ($estates as $key => $estate) {
                $slug = $estate['slug'];
                $config->estates->{$slug} = $estate;
            }
        }

        //
        // Load neighbourhoods color and label
        //
        if (!empty($neighbourhoods)) {
            foreach ($neighbourhoods as $key => $neighbourhood) {
                $slug = $estate['slug'];
                $config->neighbourhoods->{$slug} = $neighbourhood;
            }
        }

        if (!$react_property_app && $custom_app) $config->app = [ 'url' => $custom_app ];
        $config->labels = $labels;
        $config->promotions = $promotions;
        $config->enquiry_form = $enquiry_form;

        return $config;
        // echo '<script> window.traffic = ' . json_encode($config) . '; </script>';
    }

    public function admin_menu()
    {
        if (is_admin()) acf_form_head();
        // add_action('admin_enqueue_scripts', function () {
        //     if (is_admin()) acf_form_head();
        // });

        add_options_page(
            'Traffic Property App',
            'Traffic Property App',
            'manage_options',
            'traffic-property-app',
            array(
              $this,
              'settings_page'
            )
        );

        // echo '<style>.tpa_b0-p0 { padding: 0 !important; } .tpa_b0-p0 .acf-fields.-border { border: 0 !important; }</style>';
    }

    public function settings_page()
    {
        echo "<h1>Traffic Property App</h1>";

        $options = array(
          'post_id' => "traffic-property-app",
          'form' => true,
          'new_post' => false,
          'field_groups' => ['group_5dad255aaf466'],
          'return' => admin_url('admin.php?page=traffic-property-app'),
          'html_submit_button' => '<button type="submit" class="acf-button button button-primary button-large" value="Save Settings">Save Settings</button>',
        );

        acf_form($options);
    }
}


$TrafficPropertyApp = new TrafficPropertyApp();
