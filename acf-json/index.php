<?php

$tpa_acf_path = dirname(__FILE__) . '/';
$tpa_enviroment = getenv('TPA_ENVIROMENT');

function tpa_load_acf_components($paths)
{
    global $tpa_acf_path;
    $paths[] = $tpa_acf_path;
    return $paths;
}
add_filter('acf/settings/load_json', 'tpa_load_acf_components');

if ($tpa_enviroment === 'development') {
  function tpa_acf_exists($group_key){
    global $tpa_acf_path;
    return file_exists($tpa_acf_path . $group_key . '.json');
  }

  //
  // intercept acf on save
  //
  add_filter('acf/settings/save_json', function ($paths) {
      global $tpa_acf_path;
      global $post;

      $field_group = acf_get_field_group($post->ID);
      $group_key = $field_group['key'];

      return tpa_acf_exists($group_key) ? $tpa_acf_path : $paths;
  });

  //
  // Intercept acf when getting groups
  //
  add_filter('acf/get_field_group', function ($group) {
      $group_key = $group['key'];

      if (tpa_acf_exists($group_key)) $group['private'] = 0;

      return $group;
  });
}
