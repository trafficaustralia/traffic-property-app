<?php

if (!function_exists('is_plugin_active')){
  include_once ABSPATH . 'wp-admin/includes/plugin.php';
}

$includes_folder = plugin_dir_path(__FILE__);
$includes_url = plugin_dir_url(__FILE__);

//
// Load ACF plugin
//
function load_acf(){
  global $includes_folder;
  global $includes_url;
  $filename = WP_PLUGIN_DIR . '/advanced-custom-fields-pro';
  $assets_dir = $includes_folder . '/acf/';
  $assets_url = $includes_url . '/acf/';

  if (!file_exists($filename) && !is_plugin_active($filename . '/acf.php')){
    // Include the ACF plugin.
    include_once($assets_dir . 'acf.php');

    // Customize the url setting to fix incorrect asset URLs.
    add_filter('acf/settings/url', function($url) {
      global $assets_url;
      return $assets_url . $url;
    });
  }
}
load_acf();

//
// Load ACF Component fields plugin
//
function load_acf_component_fields(){
  global $includes_folder;
  $filename = WP_PLUGIN_DIR . '/acf-component-fields';
  $dir = $includes_folder . '/acf-component-fields/';

  if (!file_exists($filename) && !is_plugin_active($filename . '/acf.php')){
    // Include the ACF Component fields plugin.
    include_once($dir . 'index.php');
  }
}
load_acf_component_fields();
