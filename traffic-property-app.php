<?php
/*
Plugin Name: Traffic Property App
Description: Insite Property App for Traffic
Plugin URI: https://www.traffic.com.au
Author: Traffic Branding Agency
Author URI: https://www.traffic.com.au
Version: 1.1.18
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Include the main class.
if ( ! class_exists( 'TrafficPropertyApp' ) ) {
		include_once dirname( __FILE__ ) . '/includes/index.php';
    include_once dirname( __FILE__ ) . '/class-traffic-property-app.php';
    include_once dirname( __FILE__ ) . '/functions.php';
}
